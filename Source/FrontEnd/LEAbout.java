package FrontEnd;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * This class handles the about screen for the level editor
 */
public class LEAbout {
    @FXML
    Text txtTitle;
    @FXML
    Button btnOk;

    /**
     * Sets title font
     */
    public void initialize()    {
        txtTitle.setFont(Font.font("Skyfall", FontWeight.BOLD, 55));

    }

    /**
     * Closes the window
     */
    public void onOk()  {
        Stage thisStage = (Stage) btnOk.getScene().getWindow();
        thisStage.close();
    }
}
